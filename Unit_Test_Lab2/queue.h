/*
 * queue.h
 *
 *  Created on: Mar 1, 2019
 *      Author: kaust
 */

#ifndef QUEUE_H_
#define QUEUE_H_

// @file queue.h
#include <stdint.h>
#include <stdbool.h>
#include "stddef.h"

typedef struct {
  uint8_t queue_memory[100];
  uint8_t size;
} queue_S;

void queue__init(queue_S *queue);

bool queue__push(queue_S *queue, uint8_t push_value);

bool queue__pop(queue_S *queue, uint8_t *pop_value);

size_t queue__get_count(queue_S *queue);



#endif /* QUEUE_H_ */
