/*
 * queue.c
 *
 *  Created on: Mar 1, 2019
 *      Author: kaust
 */

#include "queue.h"

void queue__init(queue_S *queue)
{
  queue->queue_memory[0] = '\0';
  queue->size = 0;
}

bool queue__push(queue_S *queue, uint8_t push_value)
{
  if(queue->size == 99)
  {
    return false;
  }

  for(int i = queue->size; i >= 0; i--)
  {
    queue->queue_memory[i+1] = queue->queue_memory[i];
  }

  queue->queue_memory[0] = push_value;
  queue->size++;

  return true;
}

bool queue__pop(queue_S *queue, uint8_t *pop_value)
{
  if(queue->size == 0)
  {
    return false; //nothing to pop
  }

  *pop_value = queue->queue_memory[(queue->size)-1];
  queue->size--;
  return true;
}

size_t queue__get_count(queue_S *queue)
{
  return queue->size;
}

