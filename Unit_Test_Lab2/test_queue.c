#include "unity.h"
#include "queue.h"

queue_S myQue;

void test_queue__init(void)
{
  queue__init(&myQue);
  TEST_ASSERT_EQUAL('\0', myQue.queue_memory[0]);
  TEST_ASSERT_EQUAL(0, queue__get_count(&myQue));
}

void test_queue__push(void)
{

    for(int i=0; i<99; i++)
    {
		queue__push(&myQue,i);
		TEST_ASSERT_EQUAL(i, myQue.queue_memory[0]);  
    }

    TEST_ASSERT_EQUAL(99, queue__get_count(&myQue));
    
    TEST_ASSERT_FALSE(queue__push(&myQue,25));
}

void test_queue__pop(void)
{
	uint8_t poppedChar;

	for(int i=0; i<99; i++)
	{
		queue__pop(&myQue, &poppedChar);
		TEST_ASSERT_EQUAL(i,poppedChar);
	}

	TEST_ASSERT_EQUAL(0, queue__get_count(&myQue));

	TEST_ASSERT_FALSE(queue__pop(&myQue, &poppedChar));
}

void test_queue__get_count(void)
{
	TEST_ASSERT_EQUAL(0, queue__get_count(&myQue));
}