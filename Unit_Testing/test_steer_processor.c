#include "unity.h"
#include "steer_processor.h"

#include "Mocksteering.h"

void test_steer_processor__move_left(void) {
	steer_left_Expect();
	steer_processor(60,40);

	steer_left_Expect();
	steer_processor(70,45);
}

void test_steer_processor__move_right(void) {
	steer_right_Expect();
	steer_processor(40,60);

	steer_right_Expect();
	steer_processor(45,55);
}

void test_steer_processor__both_sensors_less_than_threshold(void) {
	steer_right_Expect();
  	steer_processor(48, 49);

	steer_left_Expect();
  	steer_processor(49, 48);


}

void test_steer_processor__do_not_move(void) {
	steer_processor(30,30);
	steer_processor(50,50);
	steer_processor(65,65);
}

// Do not modify this test case
// Modify your implementation of steer_processor() to make it pass
void test_steer_processor(void) {
  steer_right_Expect();
  steer_processor(10, 20);

  steer_left_Expect();
  steer_processor(20, 10);
}