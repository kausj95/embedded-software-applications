/*
 * Can_tx.c
 *
 *  Created on: Mar 5, 2019
 *      Author: kaust
 */

#include "Can_Operation.h"
#include <stdbool.h>

static int counter = 0;

bool Can_tx(can_t can)
{
    SENSOR_BUTTON_t sensorTx = {0};
    if(counter)
    {
        sensorTx.SENSOR_BUTTON_status = 1;
        sensorTx.SENSOR_BUTTON_msg = 0xAA;
        dbc_encode_and_send_SENSOR_BUTTON(&sensorTx);
    }
    else
    {
        sensorTx.SENSOR_BUTTON_status = 0;
        sensorTx.SENSOR_BUTTON_msg = 0xBB;
    }
//    printf("txs\n");
    return true;
}

//const uint32_t                             SENSOR_BUTTON__MIA_MS = 3000;
//const SENSOR_BUTTON_t                      SENSOR_BUTTON__MIA_MSG = {0};

const uint32_t                             BUTTON__MIA_MS = 3000;
const BUTTON_t                             BUTTON__MIA_MSG = {0};

void Can_rx(can_t can)
{
    BUTTON_t canButton = {0};
    can_msg_t can_msg;
    printf("lol\n");
    if(CAN_rx(can1,&can_msg,0))
    {
        printf("in rx\n");
        dbc_msg_hdr_t can_msg_hdr;
        can_msg_hdr.dlc = can_msg.frame_fields.data_len;
        can_msg_hdr.mid = can_msg.msg_id;
        printf("msg id: %d\n",can_msg.msg_id);
        switch(can_msg.msg_id){
            case 111:
                dbc_decode_BUTTON(&canButton, can_msg.data.bytes, &can_msg_hdr);
                break;

            default:
                printf("Errr!\n");
                break;
        }
    }

//    printf("message received:-%x\n",(int)msg.data.qword);
    if(1 == canButton.BUTTON_is_pushed)
    {
        counter++;
        printf("message received:-%d\n", canButton.BUTTON_msg);
        ledOn(1);
    }
    else if(0 == canButton.BUTTON_is_pushed){
        counter = 0;
        printf("message received:-%d\n", canButton.BUTTON_msg);
        ledOff(1);
    }

    dbc_handle_mia_BUTTON(&canButton, 10);
}


//bool Can_tx(can_t can)
//{
//    can_msg_t tx_msg;
//    tx_msg.msg_id = 0x100;
//    tx_msg.frame_fields.is_29bit = 1; //1 if the 11-bit ID
//    tx_msg.frame_fields.data_len = 8;
//    tx_msg.data.qword = 0x1122334455667788;
//    if(readSwitch(1))
//    {
//       tx_msg.data.bytes[0] = 0xAA;
//    }
//    else
//    {
//        tx_msg.data.bytes[0] = 0x00;
//        if(CAN_tx(can1, &tx_msg, 0))
//        {
//            printf("Switch not pressed ");
//        }
//    }
//    if(CAN_tx(can1, &tx_msg, 0))
//    {
//        return true;
//    }
//    else
//        return false;
//}
//
//
//bool Can_rx(can_t can)
//{
//    can_msg_t rx_msg;
//    if(CAN_rx(can1,&rx_msg,0))
//    {
//        if(rx_msg.data.bytes[0] == 0xAA)
//        {
//            printf("yaay!!\n");
//            ledOn(1);
//        }
//        else
//        {
//            printf("ohhh!\n");
//            ledOff(0);
//        }
//    }
//    return true;
//}
