/*
 * Can_tx.h
 *
 *  Created on: Mar 5, 2019
 *      Author: kaust
 */

#ifndef CAN_OPERATION_H_
#define CAN_OPERATION_H_

#include "can.h"
#include "switch_led.h"
#include "_can_dbc/generated_can.h"

bool Can_tx(can_t can);
void Can_rx(can_t can);

#endif /* CAN_OPERATION_H_ */
