/*
 * Can_busOff_rem.h
 *
 *  Created on: Mar 3, 2019
 *      Author: kaust
 */

#ifndef CAN_BUSOFF_REM_H_
#define CAN_BUSOFF_REM_H_

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include "can.h"

//extern can_t myCan;

void bus_off_cb(can_t can);


#endif /* CAN_BUSOFF_REM_H_ */
