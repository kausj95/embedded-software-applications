/// DBC file: ../_can_dbc/243.dbc    Self node: 'SENSOR'  (ALL = 0)
/// This file can be included by a source file, for example: #include "generated.h"
#ifndef __GENEARTED_DBC_PARSER
#define __GENERATED_DBC_PARSER
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>



/// Extern function needed for dbc_encode_and_send()
extern bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8]);

/// Missing in Action structure
typedef struct {
    uint32_t is_mia : 1;          ///< Missing in action flag
    uint32_t mia_counter_ms : 31; ///< Missing in action counter
} dbc_mia_info_t;

/// CAN message header structure
typedef struct { 
    uint32_t mid; ///< Message ID of the message
    uint8_t  dlc; ///< Data length of the message
} dbc_msg_hdr_t; 

// static const dbc_msg_hdr_t DBC_TEST1_HDR =                        {  500, 8 };
// static const dbc_msg_hdr_t DBC_TEST2_HDR =                        {  501, 8 };
// static const dbc_msg_hdr_t DBC_TEST3_HDR =                        {  502, 8 };
static const dbc_msg_hdr_t DRIVER_HEARTBEAT_HDR =                 {  100, 1 };
// static const dbc_msg_hdr_t MOTOR_CMD_HDR =                        {  101, 1 };
// static const dbc_msg_hdr_t MOTOR_STATUS_HDR =                     {  400, 3 };
static const dbc_msg_hdr_t SENSOR_SONARS_HDR =                    {  200, 8 };
static const dbc_msg_hdr_t BUTTON_HDR =                           {  111, 2 };
static const dbc_msg_hdr_t SENSOR_BUTTON_HDR =                    {  211, 2 };
// static const dbc_msg_hdr_t DBC_TEST4_HDR =                        {  503, 8 };

/// Enumeration(s) for Message: 'DRIVER_HEARTBEAT' from 'DRIVER'
typedef enum {
    DRIVER_HEARTBEAT_cmd_REBOOT = 2,
    DRIVER_HEARTBEAT_cmd_SYNC = 1,
    DRIVER_HEARTBEAT_cmd_NOOP = 0,
} DRIVER_HEARTBEAT_cmd_E ;




/// Message: DRIVER_HEARTBEAT from 'DRIVER', DLC: 1 byte(s), MID: 100
typedef struct {
    DRIVER_HEARTBEAT_cmd_E DRIVER_HEARTBEAT_cmd; ///< B7:0   Destination: SENSOR,MOTOR

    dbc_mia_info_t mia_info;
} DRIVER_HEARTBEAT_t;

/// @{ MUX'd message: SENSOR_SONARS

/// Struct for MUX: m0 (used for transmitting)
typedef struct {
    uint16_t SENSOR_SONARS_err_count;         ///< B15:4   Destination: DRIVER,IO
    float SENSOR_SONARS_left;                 ///< B27:16   Destination: DRIVER,IO
    float SENSOR_SONARS_middle;               ///< B39:28   Destination: DRIVER,IO
    float SENSOR_SONARS_right;                ///< B51:40   Destination: DRIVER,IO
    float SENSOR_SONARS_rear;                 ///< B63:52   Destination: DRIVER,IO

    // No dbc_mia_info_t for a message that we will send
} SENSOR_SONARS_m0_t;

/// Struct for MUX: m1 (used for transmitting)
typedef struct {
    uint16_t SENSOR_SONARS_err_count;         ///< B15:4   Destination: DRIVER,IO
    float SENSOR_SONARS_no_filt_left;         ///< B27:16   Destination: DBG
    float SENSOR_SONARS_no_filt_middle;       ///< B39:28   Destination: DBG
    float SENSOR_SONARS_no_filt_right;        ///< B51:40   Destination: DBG
    float SENSOR_SONARS_no_filt_rear;         ///< B63:52   Destination: DBG

    // No dbc_mia_info_t for a message that we will send
} SENSOR_SONARS_m1_t;

/// Struct with all the child MUX'd signals (Used for receiving)
typedef struct {
    SENSOR_SONARS_m0_t m0; ///< MUX'd structure
    SENSOR_SONARS_m1_t m1; ///< MUX'd structure
} SENSOR_SONARS_t;
/// @} MUX'd message


/// Message: BUTTON from 'DRIVER', DLC: 2 byte(s), MID: 111
typedef struct {
    uint8_t BUTTON_is_pushed : 1;             ///< B0:0   Destination: SENSOR
    uint8_t BUTTON_msg;                       ///< B15:8   Destination: SENSOR

    dbc_mia_info_t mia_info;
} BUTTON_t;


/// Message: SENSOR_BUTTON from 'SENSOR', DLC: 2 byte(s), MID: 211
typedef struct {
    uint8_t SENSOR_BUTTON_status : 1;         ///< B0:0   Destination: DRIVER
    uint8_t SENSOR_BUTTON_msg;                ///< B15:8   Destination: DRIVER

    // No dbc_mia_info_t for a message that we will send
} SENSOR_BUTTON_t;


/// @{ These 'externs' need to be defined in a source file of your project
extern const uint32_t                             DRIVER_HEARTBEAT__MIA_MS;
extern const DRIVER_HEARTBEAT_t                   DRIVER_HEARTBEAT__MIA_MSG;
extern const uint32_t                             BUTTON__MIA_MS;
extern const BUTTON_t                             BUTTON__MIA_MSG;
/// @}


/// Not generating code for dbc_encode_DBC_TEST1() since the sender is IO and we are SENSOR

/// Not generating code for dbc_encode_DBC_TEST2() since the sender is IO and we are SENSOR

/// Not generating code for dbc_encode_DBC_TEST3() since the sender is IO and we are SENSOR

/// Not generating code for dbc_encode_DRIVER_HEARTBEAT() since the sender is DRIVER and we are SENSOR

/// Not generating code for dbc_encode_MOTOR_CMD() since the sender is DRIVER and we are SENSOR

/// Not generating code for dbc_encode_MOTOR_STATUS() since the sender is MOTOR and we are SENSOR

/// Encode SENSOR's 'SENSOR_SONARS' MUX(m0) message
/// @returns the message header of this message
static inline dbc_msg_hdr_t dbc_encode_SENSOR_SONARS_m0(uint8_t bytes[8], SENSOR_SONARS_m0_t *from)
{
    uint32_t raw;
    bytes[0]=bytes[1]=bytes[2]=bytes[3]=bytes[4]=bytes[5]=bytes[6]=bytes[7]=0;

    // Set the MUX index value
    raw = ((uint32_t)(((0)))) & 0x0f;
    bytes[0] |= (((uint8_t)(raw) & 0x0f)); ///< 4 bit(s) starting from B0

    // Set non MUX'd signals that need to go out with this MUX'd message
    raw = ((uint32_t)(((from->SENSOR_SONARS_err_count)))) & 0xfff;
    bytes[0] |= (((uint8_t)(raw) & 0x0f) << 4); ///< 4 bit(s) starting from B4
    bytes[1] |= (((uint8_t)(raw >> 4) & 0xff)); ///< 8 bit(s) starting from B8

    // Set the rest of the signals within this MUX (m0)
    raw = ((uint32_t)(((from->SENSOR_SONARS_left) / 0.1) + 0.5)) & 0xfff;
    bytes[2] |= (((uint8_t)(raw) & 0xff)); ///< 8 bit(s) starting from B16
    bytes[3] |= (((uint8_t)(raw >> 8) & 0x0f)); ///< 4 bit(s) starting from B24
    raw = ((uint32_t)(((from->SENSOR_SONARS_middle) / 0.1) + 0.5)) & 0xfff;
    bytes[3] |= (((uint8_t)(raw) & 0x0f) << 4); ///< 4 bit(s) starting from B28
    bytes[4] |= (((uint8_t)(raw >> 4) & 0xff)); ///< 8 bit(s) starting from B32
    raw = ((uint32_t)(((from->SENSOR_SONARS_right) / 0.1) + 0.5)) & 0xfff;
    bytes[5] |= (((uint8_t)(raw) & 0xff)); ///< 8 bit(s) starting from B40
    bytes[6] |= (((uint8_t)(raw >> 8) & 0x0f)); ///< 4 bit(s) starting from B48
    raw = ((uint32_t)(((from->SENSOR_SONARS_rear) / 0.1) + 0.5)) & 0xfff;
    bytes[6] |= (((uint8_t)(raw) & 0x0f) << 4); ///< 4 bit(s) starting from B52
    bytes[7] |= (((uint8_t)(raw >> 4) & 0xff)); ///< 8 bit(s) starting from B56

    return SENSOR_SONARS_HDR;
}

/// Encode and send for dbc_encode_SENSOR_SONARS_m0() message
static inline bool dbc_encode_and_send_SENSOR_SONARS_m0(SENSOR_SONARS_m0_t *from)
{
    uint8_t bytes[8];
    const dbc_msg_hdr_t hdr = dbc_encode_SENSOR_SONARS_m0(bytes, from);
    return dbc_app_send_can_msg(hdr.mid, hdr.dlc, bytes);
}


/// Encode SENSOR's 'SENSOR_SONARS' MUX(m1) message
/// @returns the message header of this message
static inline dbc_msg_hdr_t dbc_encode_SENSOR_SONARS_m1(uint8_t bytes[8], SENSOR_SONARS_m1_t *from)
{
    uint32_t raw;
    bytes[0]=bytes[1]=bytes[2]=bytes[3]=bytes[4]=bytes[5]=bytes[6]=bytes[7]=0;

    // Set the MUX index value
    raw = ((uint32_t)(((1)))) & 0x0f;
    bytes[0] |= (((uint8_t)(raw) & 0x0f)); ///< 4 bit(s) starting from B0

    // Set non MUX'd signals that need to go out with this MUX'd message
    raw = ((uint32_t)(((from->SENSOR_SONARS_err_count)))) & 0xfff;
    bytes[0] |= (((uint8_t)(raw) & 0x0f) << 4); ///< 4 bit(s) starting from B4
    bytes[1] |= (((uint8_t)(raw >> 4) & 0xff)); ///< 8 bit(s) starting from B8

    // Set the rest of the signals within this MUX (m1)
    raw = ((uint32_t)(((from->SENSOR_SONARS_no_filt_left) / 0.1) + 0.5)) & 0xfff;
    bytes[2] |= (((uint8_t)(raw) & 0xff)); ///< 8 bit(s) starting from B16
    bytes[3] |= (((uint8_t)(raw >> 8) & 0x0f)); ///< 4 bit(s) starting from B24
    raw = ((uint32_t)(((from->SENSOR_SONARS_no_filt_middle) / 0.1) + 0.5)) & 0xfff;
    bytes[3] |= (((uint8_t)(raw) & 0x0f) << 4); ///< 4 bit(s) starting from B28
    bytes[4] |= (((uint8_t)(raw >> 4) & 0xff)); ///< 8 bit(s) starting from B32
    raw = ((uint32_t)(((from->SENSOR_SONARS_no_filt_right) / 0.1) + 0.5)) & 0xfff;
    bytes[5] |= (((uint8_t)(raw) & 0xff)); ///< 8 bit(s) starting from B40
    bytes[6] |= (((uint8_t)(raw >> 8) & 0x0f)); ///< 4 bit(s) starting from B48
    raw = ((uint32_t)(((from->SENSOR_SONARS_no_filt_rear) / 0.1) + 0.5)) & 0xfff;
    bytes[6] |= (((uint8_t)(raw) & 0x0f) << 4); ///< 4 bit(s) starting from B52
    bytes[7] |= (((uint8_t)(raw >> 4) & 0xff)); ///< 8 bit(s) starting from B56

    return SENSOR_SONARS_HDR;
}

/// Encode and send for dbc_encode_SENSOR_SONARS_m1() message
static inline bool dbc_encode_and_send_SENSOR_SONARS_m1(SENSOR_SONARS_m1_t *from)
{
    uint8_t bytes[8];
    const dbc_msg_hdr_t hdr = dbc_encode_SENSOR_SONARS_m1(bytes, from);
    return dbc_app_send_can_msg(hdr.mid, hdr.dlc, bytes);
}



/// Not generating code for dbc_encode_BUTTON() since the sender is DRIVER and we are SENSOR

/// Encode SENSOR's 'SENSOR_BUTTON' message
/// @returns the message header of this message
static inline dbc_msg_hdr_t dbc_encode_SENSOR_BUTTON(uint8_t bytes[8], SENSOR_BUTTON_t *from)
{
    uint32_t raw;
    bytes[0]=bytes[1]=bytes[2]=bytes[3]=bytes[4]=bytes[5]=bytes[6]=bytes[7]=0;

    raw = ((uint32_t)(((from->SENSOR_BUTTON_status)))) & 0x01;
    bytes[0] |= (((uint8_t)(raw) & 0x01)); ///< 1 bit(s) starting from B0

    raw = ((uint32_t)(((from->SENSOR_BUTTON_msg)))) & 0xff;
    bytes[1] |= (((uint8_t)(raw) & 0xff)); ///< 8 bit(s) starting from B8

    return SENSOR_BUTTON_HDR;
}

/// Encode and send for dbc_encode_SENSOR_BUTTON() message
static inline bool dbc_encode_and_send_SENSOR_BUTTON(SENSOR_BUTTON_t *from)
{
    uint8_t bytes[8];
    const dbc_msg_hdr_t hdr = dbc_encode_SENSOR_BUTTON(bytes, from);
    return dbc_app_send_can_msg(hdr.mid, hdr.dlc, bytes);
}



/// Not generating code for dbc_encode_DBC_TEST4() since the sender is IO and we are SENSOR

/// Not generating code for dbc_decode_DBC_TEST1() since 'SENSOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_DBC_TEST2() since 'SENSOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_DBC_TEST3() since 'SENSOR' is not the recipient of any of the signals

/// Decode DRIVER's 'DRIVER_HEARTBEAT' message
/// @param hdr  The header of the message to validate its DLC and MID; this can be NULL to skip this check
static inline bool dbc_decode_DRIVER_HEARTBEAT(DRIVER_HEARTBEAT_t *to, const uint8_t bytes[8], const dbc_msg_hdr_t *hdr)
{
    const bool success = true;
    // If msg header is provided, check if the DLC and the MID match
    if (NULL != hdr && (hdr->dlc != DRIVER_HEARTBEAT_HDR.dlc || hdr->mid != DRIVER_HEARTBEAT_HDR.mid)) {
        return !success;
    }

    uint32_t raw;
    raw  = ((uint32_t)((bytes[0]))); ///< 8 bit(s) from B0
    to->DRIVER_HEARTBEAT_cmd = (DRIVER_HEARTBEAT_cmd_E)((raw));

    to->mia_info.mia_counter_ms = 0; ///< Reset the MIA counter

    return success;
}


/// Not generating code for dbc_decode_MOTOR_CMD() since 'SENSOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_MOTOR_STATUS() since 'SENSOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_SENSOR_SONARS() since 'SENSOR' is not the recipient of any of the signals

/// Decode DRIVER's 'BUTTON' message
/// @param hdr  The header of the message to validate its DLC and MID; this can be NULL to skip this check
static inline bool dbc_decode_BUTTON(BUTTON_t *to, const uint8_t bytes[8], const dbc_msg_hdr_t *hdr)
{
    const bool success = true;
    // If msg header is provided, check if the DLC and the MID match
    if (NULL != hdr && (hdr->dlc != BUTTON_HDR.dlc || hdr->mid != BUTTON_HDR.mid)) {
        return !success;
    }

    uint32_t raw;
    raw  = ((uint32_t)((bytes[0]) & 0x01)); ///< 1 bit(s) from B0
    to->BUTTON_is_pushed = ((raw));
    raw  = ((uint32_t)((bytes[1]))); ///< 8 bit(s) from B8
    to->BUTTON_msg = ((raw));

    to->mia_info.mia_counter_ms = 0; ///< Reset the MIA counter

    return success;
}


/// Not generating code for dbc_decode_SENSOR_BUTTON() since 'SENSOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_DBC_TEST4() since 'SENSOR' is not the recipient of any of the signals

/// Handle the MIA for DRIVER's DRIVER_HEARTBEAT message
/// @param   time_incr_ms  The time to increment the MIA counter with
/// @returns true if the MIA just occurred
/// @post    If the MIA counter reaches the MIA threshold, MIA struct will be copied to *msg
static inline bool dbc_handle_mia_DRIVER_HEARTBEAT(DRIVER_HEARTBEAT_t *msg, uint32_t time_incr_ms)
{
    bool mia_occurred = false;
    const dbc_mia_info_t old_mia = msg->mia_info;
    msg->mia_info.is_mia = (msg->mia_info.mia_counter_ms >= DRIVER_HEARTBEAT__MIA_MS);

    if (!msg->mia_info.is_mia) { // Not MIA yet, so keep incrementing the MIA counter
        msg->mia_info.mia_counter_ms += time_incr_ms;
    }
    else if(!old_mia.is_mia)   { // Previously not MIA, but it is MIA now
        // Copy MIA struct, then re-write the MIA counter and is_mia that is overwriten
        *msg = DRIVER_HEARTBEAT__MIA_MSG;
        msg->mia_info.mia_counter_ms = DRIVER_HEARTBEAT__MIA_MS;
        msg->mia_info.is_mia = true;
        mia_occurred = true;
    }

    return mia_occurred;
}

/// Handle the MIA for DRIVER's BUTTON message
/// @param   time_incr_ms  The time to increment the MIA counter with
/// @returns true if the MIA just occurred
/// @post    If the MIA counter reaches the MIA threshold, MIA struct will be copied to *msg
static inline bool dbc_handle_mia_BUTTON(BUTTON_t *msg, uint32_t time_incr_ms)
{
    bool mia_occurred = false;
    const dbc_mia_info_t old_mia = msg->mia_info;
    msg->mia_info.is_mia = (msg->mia_info.mia_counter_ms >= BUTTON__MIA_MS);

    if (!msg->mia_info.is_mia) { // Not MIA yet, so keep incrementing the MIA counter
        msg->mia_info.mia_counter_ms += time_incr_ms;
    }
    else if(!old_mia.is_mia)   { // Previously not MIA, but it is MIA now
        // Copy MIA struct, then re-write the MIA counter and is_mia that is overwriten
        *msg = BUTTON__MIA_MSG;
        msg->mia_info.mia_counter_ms = BUTTON__MIA_MS;
        msg->mia_info.is_mia = true;
        mia_occurred = true;
    }

    return mia_occurred;
}

#endif
