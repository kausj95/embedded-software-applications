/**
 * @file
 *
 * The purpose of this "C" callbacks is to provide the code to be able
 * to call pure C functions and unit-test it in C test framework
 */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

//#include "printf_lib.h"
//#include "utilities.h"
#include "c_period_callbacks.h"
#include "c_uart2.h"
#include "string.h"

char txQue[5],rxQue[5];
int temperature = 0;

/// Called once before the RTOS is started, this is a good place to initialize things once
bool c_period_init(void)
{
    uart_init(9600, 100);
    return true; // Must return true upon success
}

/// Register any telemetry variables
bool c_period_reg_tlm(void)
{
    // Make sure "SYS_CFG_ENABLE_TLM" is enabled at sys_config.h to use Telemetry
    return true; // Must return true upon success
}


/**
 * Below are your periodic functions.
 * The argument 'count' is the number of times each periodic task is called.
 */

void c_period_1Hz(uint32_t count)
{
//    LE.toggle(1);
//    printf(" 1 \n");
    (void) count;
}

void c_period_10Hz(uint32_t count)
{
    (void) count;
#if 0
//    LE.toggle(2);
    sprintf(txQue,"%.f",temp);
    uart_putline(txQue,10);
    if(uart_gets(rxQue,sizeof(rxQue),30))
    {
        printf("received\n");
//        for(int i=0; i<sizeof(temp); i++)
//        {
//            printf("%c",rxQue[i]);
//
//        }
        printf("\n");
        temperature = atoi(rxQue);
        printf("LD = %d\n",temperature);
    }
#endif
//    printf(" 10 \n");
}

void c_period_100Hz(uint32_t count)
{
//    printf(" 100 \n");
//    LE.toggle(3);
    (void) count;
}

// 1Khz (1ms) is only run if Periodic Dispatcher was configured to run it at main():
// scheduler_add_task(new periodicSchedulerTask(run_1Khz = true));
void c_period_1000Hz(uint32_t count)
{
//    printf(" 1000 \n");
//    LE.toggle(4);
    (void) count;
}
