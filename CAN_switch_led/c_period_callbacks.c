/**
 * @file
 *
 * The purpose of this "C" callbacks is to provide the code to be able
 * to call pure C functions and unit-test it in C test framework
 */

#include <stdint.h>
#include <stdbool.h>
#include "stddef.h"
#include "c_period_callbacks.h"
#include "can.h"
#include "Can_busOff_rem.h"
#include "Can_Operation.h"

bool C_period_init(void) {
    printf("in init\n");
    if(CAN_init(can1,100,32,32,(void*)bus_off_cb,NULL))
    {
        CAN_bypass_filter_accept_all_msgs();
        CAN_reset_bus(can1);

        return true;
    }

    return false;
}

bool C_period_reg_tlm(void) {
    return true;
}

void C_period_1Hz(uint32_t count) {
    (void) count;
    if(CAN_is_bus_off(can1))
    {
        bus_off_cb(can1);
    }
}

void C_period_10Hz(uint32_t count) {
    (void) count;
}

void C_period_100Hz(uint32_t count) {
    (void) count;
    Can_tx(can1);
    Can_rx(can1);
}

void C_period_1000Hz(uint32_t count) {
    (void) count;
}
