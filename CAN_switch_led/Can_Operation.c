/*
 * Can_tx.c
 *
 *  Created on: Mar 5, 2019
 *      Author: kaust
 */

#include "Can_Operation.h"
#include <stdbool.h>

can_msg_t msg;

bool Can_tx(can_t can)
{
    msg.msg_id = 0x123;
    //msg.frame_fields.is_29bit = 1;
    msg.frame_fields.data_len = 8;
    msg.data.qword = 0x1122334455667788;
    if(readSwitch(2))
    {
        msg.data.qword = 0xAA;
    }
    else
    {
        msg.data.qword = 0x00;
    }
    if(CAN_tx(can,&msg,0))
    {
        return true;
    }
    else
        return false;
}

bool Can_rx(can_t can)
{
    if(CAN_rx(can,&msg,0))
    {
        printf("message received:-%x\n",(int)msg.data.qword);
        if(msg.data.qword == 0xAA)
        {
            ledOn(1);
        }
        else
            ledOff(1);
        return true;
    }
    else
        return false;
}
