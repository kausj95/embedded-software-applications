#include "unity.h" // Single Unity Test Framework include
#include <stdio.h>

#include "c_period_callbacks.h"
#include "MockCan_busOff_rem.h"
#include "MockCan_Operation.h"
#include "Mockcan.h"

//#include "io.hpp"

// Mock the UART2 C header API
//#include "Mockc_uart2.h"

can_t myCan = can1;

void setUp(void) {
}
void tearDown(void) {
}

void test_C_period_init(void) {
  CAN_init_ExpectAndReturn(myCan,100,32,32,(void*)bus_off_cb,NULL,true);
  CAN_bypass_filter_accept_all_msgs_Expect();
  CAN_reset_bus_Expect(myCan);
  C_period_init();


CAN_init_ExpectAndReturn(myCan,100,32,32,(void*)bus_off_cb,NULL,false);
 // CAN_bypass_filter_accept_all_msgs_Expect();
 // CAN_reset_bus_Expect(myCan);
  C_period_init();


}

void test_C_period_1Hz(void) {
    CAN_is_bus_off_ExpectAndReturn(myCan,true);
    bus_off_cb_Expect(myCan);
    C_period_1Hz(1);
}

void test_C_period_100Hz(void) {
    
    Can_tx_ExpectAndReturn(myCan,true);
    Can_rx_ExpectAndReturn(myCan,true);
    C_period_100Hz(1);
}