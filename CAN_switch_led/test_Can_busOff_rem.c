#include "unity.h" // Single Unity Test Framework include
#include <stdio.h>

#include "Can_busOff_rem.h"
#include "Mockcan.h"

can_t myCan = can1;

void test_bus_off_cb(void) {
  CAN_reset_bus_Expect(myCan);
  CAN_reset_bus(myCan);
}
