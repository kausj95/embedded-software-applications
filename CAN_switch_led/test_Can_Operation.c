#include "unity.h" // Single Unity Test Framework include
#include <stdio.h>

#include "Can_Operation.h"
#include "Mockcan.h"
#include "Mockswitch_led.h"

can_t myCan = can1;
int pinNum = 1;
can_msg_t msg;

void test_Can_tx(void) {
  	readSwitch_ExpectAndReturn(pinNum,true);
	CAN_tx_ExpectAndReturn(myCan,&msg,0,true);
	Can_tx(myCan);	

	//TEST_ASSERT_TRUE(readSwitch(0));
	
	//ledOn_Expect(pinNum);
	
}

void test_Can_rx(void) {
	CAN_rx_ExpectAndReturn(myCan,&msg,0,true);
	ledOn_Expect(pinNum);
	Can_rx(myCan);
}
