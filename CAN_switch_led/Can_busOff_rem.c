/*
 * Can_busOff_rem.c
 *
 *  Created on: Mar 3, 2019
 *      Author: kaust
 */

#include "Can_busOff_rem.h"

void bus_off_cb(can_t can)
{
    CAN_reset_bus(can);
}


